#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Party(ModelSQL, ModelView):
    _name = 'party.party'

    collective_invoice_recipient = fields.Property(fields.Many2One(
            'party.party', 'Collective Invoice Recipient',
            help='The invoice recipient for collective invoices.'))

    supplier_collective_invoice_recipient = fields.Property(fields.Many2One(
            'party.party', 'Supplier Collective Invoice Recipient',
            help='The invoice recipient for collective invoices.'))

Party()
