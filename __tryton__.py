# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Collection',
    'name_de_DE': 'Fakturierung Sammelrechnung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Collective Invoices for Accounting
    - Provides the possibility to configure collective billing on Payment Terms
''',
    'description_de_DE': '''Sammelrechnungen in der Fakturierung
    - Ermöglicht die Konfiguration von Sammelrechnungen auf den
      Zahlungsbedingungen
''',
    'depends': [
        'account_invoice',
        'account_invoice_time_supply'
    ],
    'xml': [
        'invoice.xml',
        'party.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
