# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
import copy
from dateutil.relativedelta import relativedelta
from trytond.model import ModelWorkflow, ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Equal, Eval, Not, In, Or
from trytond.pool import Pool
from trytond.transaction import Transaction


_INVOICE_SETTLEMENT_TYPES = [
    ('individual', 'Individual Settlement'),
    ('collective', 'Collective Settlement'),
    ('voucher', 'Voucher'),
    ('draft', 'Draft')
    ]

class Invoice(ModelWorkflow, ModelSQL, ModelView):
    _name = 'account.invoice'

    settlement_type = fields.Selection(_INVOICE_SETTLEMENT_TYPES,
            'Settlement Type', required=True, readonly=True, select=2)
    collection = fields.Many2One('account.invoice', 'Collection',
            domain=[('settlement_type', '=', 'collective')],
            states={
                'invisible': Not(Equal(Eval('settlement_type'), 'voucher'))
                }, readonly=True, depends=['settlement_type'])
    collection_vouchers = fields.One2Many('account.invoice', 'collection',
            'Collection Vouchers',
            domain=[('settlement_type', '=', 'voucher')],
            states={
                'invisible': Not(Equal(Eval('settlement_type'), 'collective'))
                }, readonly=True, depends=['settlement_type'])
    collection_open = fields.Function(fields.Boolean('Collection Open'),
            'get_collection_open')

    def __init__(self):
        super(Invoice, self).__init__()
        self._error_messages.update({
            'party_payment_term_missing': 'On Party (%s) %s ' \
            'is no payment term or supplier payment term set. ' \
            'Please correct this.',
            })
        self.lines.states = copy.copy(self.lines.states)
        readonly = Equal(Eval('settlement_type'), 'collective')
        if not self.lines.states.get('readonly'):
            self.lines.states['readonly'] = readonly
        else:
            self.lines.states['readonly'] = (Or(
                self.lines.states['readonly'], readonly))
        if 'settlement_type' not in self.lines.depends:
            self.lines.depends = copy.copy(self.lines.depends)
            self.lines.depends.append('settlement_type')
        self.state = copy.copy(self.state)
        if ('waiting', 'Waiting') \
                not in self.state.selection:
            self.state.selection += [
                ('waiting', 'Waiting')
                ]
        if ('cleared', 'Cleared') not in self.state.selection:
            self.state.selection += [('cleared', 'Cleared')]
        if ('cancel_voucher', 'Canceled') not in self.state.selection:
            self.state.selection += [('cancel_voucher', 'Canceled')]
        self.invoice_date.states = copy.copy(self.invoice_date.states)
        readonly = In(Eval('state'), [
            'open', 'paid', 'cancel', 'waiting', 'cleared', 'cancel_voucher'])
        required = In(Eval('state'), ['open', 'paid'])
        if not self.invoice_date.states.get('readonly'):
            self.invoice_date.states['readonly'] = readonly
        else:
            self.invoice_date.states['readonly'] = (Or(
                self.invoice_date.states['readonly'], readonly))
        if not self.invoice_date.states.get('required'):
            self.invoice_date.states['required'] = required
        else:
            self.invoice_date.states['required'] = (Or(
                self.invoice_date.states['required'], required))
        if 'state' not in self.invoice_date.depends:
            self.invoice_date.depends = copy.copy(self.invoice_date.depends)
            self.invoice_date.depends.append('state')

        self._reset_columns()

    def init(self, module_name):
        super(Invoice, self).init(module_name)
        cursor = Transaction().cursor
        # Migration from 2.0 canceled vouchers now have state 'cancel_voucher'
        cursor.execute('UPDATE "%s" SET "state" = %%s '
            'WHERE "state" = %%s '
                'AND "settlement_type" = %%s' % self._table,
            ('cancel_voucher', 'cancel', 'voucher'))
        # Migration from 2.0 confirmed vouchers now have workflow completed
        cursor.execute('UPDATE "wkf_instance" SET "state" = %%s '
            'WHERE "res_id" IN ('
                'SELECT id from "%s" '
                'WHERE "state" = %%s '
                'AND "settlement_type" = %%s)' % self._table,
            ('complete', 'waiting', 'voucher'))
        # Migration from 2.0 transition must not be run on workflow migration
        cursor.execute('DELETE FROM "wkf_transition" '
            'WHERE act_to IN ('
                'SELECT id FROM "wkf_activity" WHERE "name" = %s)',
            ('Set Collection Vouchers Cleared',))

    def default_settlement_type(self):
        return 'draft'

    def get_collection_open(self, ids, name):
        res = {}
        for invoice in self.browse(ids):
            res[invoice.id] = False
            if invoice.collection and \
                    invoice.collection.state in ('open', 'paid'):
                res[invoice.id] = True
        return res

    def set_settlement_type(self, invoice_id):
        invoice = self.browse(invoice_id)
        if invoice.payment_term.collective_invoice and \
                invoice.settlement_type != 'collective':
            self.write(invoice_id, {'settlement_type': 'voucher'})
        elif not invoice.payment_term.collective_invoice and \
                invoice.settlement_type != 'collective':
            self.write(invoice_id, {'settlement_type': 'individual'})
        return True

    def set_collection_vouchers_cleared(self, invoice_id):
        invoice = self.browse(invoice_id)
        if invoice.state in ('open', 'paid') and \
                invoice.settlement_type == 'collective':
            self.write([x.id for x in invoice.collection_vouchers],
                {'state': 'cleared'})
        return True

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['collection'] = False
        default['collection_vouchers'] = False
        default['settlement_type'] = 'draft'
        return super(Invoice, self).copy(ids, default=default)

    def button_draft(self, ids):
        super(Invoice, self).button_draft(ids)
        self.write(ids, {
            'settlement_type': 'draft',
            'invoice_report_cache': False,
            'invoice_report_format': False,
            })
        return True

    def _search_vouchers_collective_invoice(self):
        date_obj = Pool().get('ir.date')

        today = date_obj.today()
        first_day_this_month = datetime.date(year=today.year,
                month=today.month, day=1)
        args = [
            ('invoice_date', '<', first_day_this_month),
            ('state', '=', 'waiting'),
            ('collection', '=', False)
            ]
        res = self.search(args)
        return res

    def _sort_vouchers_collective_invoice(self, invoice_ids):
        if not invoice_ids:
            return []

        vouchers = {}
        for invoice in self.browse(invoice_ids):
            vouchers.setdefault(invoice.type, {})
            vouchers[invoice.type].setdefault(invoice.company.id, {})
            party = invoice.party.id
            if invoice.type in ('in_invoice', 'in_credit_note') and \
                    invoice.party.supplier_collective_invoice_recipient:
                party = invoice.party.supplier_collective_invoice_recipient.id
            elif invoice.type in ('out_invoice', 'out_credit_note') and \
                    invoice.party.collective_invoice_recipient:
                party = invoice.party.collective_invoice_recipient.id

            vouchers[invoice.type][invoice.company.id].setdefault(
                    party, {})
            vouchers[invoice.type][invoice.company.id][
                    party].setdefault(invoice.currency.id, {})
            effective_date = invoice.time_of_supply_end or \
                    invoice.accounting_date or invoice.invoice_date
            vouchers[invoice.type][invoice.company.id][
                    party][invoice.currency.id].setdefault((
                    effective_date.month, effective_date.year), [])
            vouchers[invoice.type][invoice.company.id][party][
                    invoice.currency.id][(effective_date.month,
                    effective_date.year)].append(invoice.id)
        return vouchers

    def create_collective_invoice(self):
        pool = Pool()
        journal_obj = pool.get('account.journal')
        invoice_line_obj = pool.get('account.invoice.line')
        party_obj = pool.get('party.party')

        new_ids = []

        invoice_ids = self._search_vouchers_collective_invoice()
        vouchers = self._sort_vouchers_collective_invoice(invoice_ids)
        if not vouchers:
            return []

        for type in vouchers:
            for company in vouchers[type]:
                for party in vouchers[type][company]:
                    for currency in vouchers[type][company][party]:
                        for month_year in \
                                vouchers[type][company][party][currency]:
                            vals = {}
                            vals['type'] = type
                            vals['party'] = party
                            vals['currency'] = currency
                            start_date = datetime.date(year=month_year[1],
                                    month=month_year[0], day=1)
                            vals['accounting_date'] = start_date + \
                                    relativedelta(months=+1, days=-1)
                            vals['time_of_supply_start'] = start_date
                            vals['time_of_supply_end'] = start_date + \
                                    relativedelta(months=+1, days=-1)
                            vals['collection_vouchers'] = [
                                    ('set', vouchers[type][company][party][
                                        currency][month_year])
                                    ]
                            party2 = party_obj.browse(party)
                            if type in ('in_invoice', 'in_credit_note'):
                                journal_type = 'expense'
                                account = party2.account_payable.id
                                payment_term = party2.supplier_payment_term.id
                            else:
                                journal_type = 'revenue'
                                account = party2.account_receivable.id
                                payment_term = party2.payment_term.id
                            if not payment_term:
                                self.raise_user_error(
                                        'party_payment_term_missing',
                                        error_args=(party2.code,party2.name))

                            journal_id = journal_obj.search([
                                ('type', '=', journal_type),
                                ], limit=1)
                            if journal_id:
                                journal_id = journal_id[0]

                            vals['company'] = company
                            vals['journal'] = journal_id
                            vals['invoice_address'] = party_obj.address_get(
                                    party2.id, type='invoice')
                            vals['account'] = account
                            vals['payment_term'] = payment_term
                            vals['settlement_type'] = 'collective'

                            new_id = self.create(vals)
                            new_ids.append(new_id)

                            for invoice in self.browse(
                                    vouchers[type][company][party][currency][
                                        month_year]):
                                for line in invoice.lines:
                                    default = {}
                                    default['invoice'] = new_id
                                    new_line_id = invoice_line_obj.copy(
                                            line.id, default=default)
                            self.update_taxes([new_id])
        return new_ids

    def _credit(self, invoice):
        company_obj = Pool().get('company.company')

        res = super(Invoice, self)._credit(invoice)
        if res.get('company'):
            company = company_obj.browse(res['company'])
            if res.get('type') == 'out_credit_note':
                res['payment_term'] = company.payment_term.id
            elif res.get('type') == 'in_credit_note':
                res['payment_term'] = company.supplier_payment_term.id

        return res

    def wkf_invoice_activity_open_settlement_type(self, invoice):
        self.set_settlement_type(invoice.id)
        self.set_number(invoice.id)

    def wkf_invoice_activity_confirm_voucher(self, invoice):
        self.write(invoice.id, {'state': 'waiting'})
        self.print_invoice(invoice.id)

    def wkf_open(self, invoice):
        self.create_move(invoice.id)
        self.write(invoice.id, {'state': 'open'})
        self.set_collection_vouchers_cleared(invoice.id)
        self.print_invoice(invoice.id)

    def wkf_invoice_transition_open_settlement_type_open(self, invoice):
        return invoice.settlement_type != 'voucher'

    def wkf_invoice_transition_open_settlement_type_confirm_voucher(self,
            invoice):
        return invoice.settlement_type == 'voucher'

Invoice()


class PaymentTerm(ModelSQL, ModelView):
    _name = 'account.invoice.payment_term'

    collective_invoice = fields.Boolean('Collective Invoice')

    def default_collective_invoice(self):
        return False

PaymentTerm()


class CreateCollectiveInvoice(Wizard):
    'Create Collective Invoice'
    _name = 'account.invoice.create_collective_invoice'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'action',
                'action': '_create_collective_invoice',
                'state': 'end',
            },
        },
    }

    def _create_collective_invoice(self, data):
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')

        new_ids = invoice_obj.create_collective_invoice()

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_invoice_all_invoice_draft_form'),
            ('module', '=', 'account_invoice_collection'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        return res

CreateCollectiveInvoice()


class CancelVoucher(Wizard):
    'Cancel Voucher'
    _name = 'account.invoice.cancel_voucher'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'action',
                'action': '_cancel_voucher',
                'state': 'end',
            },
        },
    }

    def __init__(self):
        super(CancelVoucher, self).__init__()
        self._error_messages.update({'wrong_state': 'You can only cancel '
            "vouchers with the state 'waiting' with this wizard."})

    def _cancel_voucher(self, data):
        invoice_obj = Pool().get('account.invoice')
        cancel_ids = []
        for invoice in invoice_obj.browse(data['ids']):
            if invoice.state == 'waiting':
                cancel_ids.append(invoice.id)
            else:
                self.raise_user_error('wrong_state')
        if cancel_ids:
           invoice_obj.write(cancel_ids, {'state': 'cancel_voucher'})
        return {}

CancelVoucher()
