==========================
Account Invoice Collection
==========================

This is the documentation of functionality and usage of the
account_invoice_collection module.

A flag on payment term indicates that one or more single vouchers
should be combined in one invoice.

As a general rule this scenario uses the date of today minus 60 days as
the base date to create invoice vouchers.

.. only:: test

    .. warning:: The illustrated console examples are invasive
       and creating transactional data which can not be deleted.
       The console examples should never ran in production environments.


Modul Setup
===========

.. only:: test

    Console example::

        >>> ### Imports
        >>>
        >>> import time
        >>> import datetime
        >>> import random
        >>> from decimal import Decimal
        >>> from proteus import config, Model, Wizard
        >>> today = datetime.date.today()
        >>>
        >>> ### Connection
        >>>
        >>> URL = 'https://admin:UMK3uJHzo7Yq@localhost:57089/ewc'
        >>> config = config.set_xmlrpc(URL)
        >>>
        >>> ### Init Models
        >>>
        >>> Address = Model.get('party.address')
        >>> Account = Model.get('account.account')
        >>> DunningTerm = Model.get('account.dunning.term')
        >>> DunningLevel = Model.get('account.dunning.level')
        >>> Fiscalyear = Model.get('account.fiscalyear')
        >>> Invoice = Model.get('account.invoice')
        >>> InvoiceLine = Model.get('account.invoice.line')
        >>> Party = Model.get('party.party')
        >>> PaymentTerm = Model.get('account.invoice.payment_term')
        >>> PaymentTermLine = Model.get('account.invoice.payment_term.line')


Master Data
-----------

Make sure to run master_data.rst to rollout general preferences.


Payment Term
------------

Example 1: Lookup or create a payment term which *name* is
`21 Days net (collective)`, set up with checked field *collective invoice*
and a *line* of *type* `remainder` after `21 days net` after invoice date.

.. only:: test

    Console example::

        >>> payment_term = PaymentTerm.find([ \
        ...        ('name', '=', '21 Days net (collective)')])
        >>> if payment_term:
        ...     payment_term = payment_term[0]
        ... else:
        ...     payment_term = PaymentTerm(name='21 Days net (collective)')
        ...     payment_term.collective_invoice = True
        ...     payment_term_line = PaymentTermLine(type='remainder')
        ...     payment_term_line.days = 10
        ...     payment_term.lines.append(payment_term_line)
        ...     payment_term.save()

.. note:: If the field `collective invoice` is checked an invoice will be
    handled as a *voucher* of an invoice collection.


Customer Party
--------------

Example 2: Create a new party, with payment term from above.

.. only:: test

    Console example::

        >>> customer = Party(name='Customer %i' % (int(time.time()),))
        >>> customer.payment_term = payment_term
        >>> customer_address = Address(city="Lunar City")
        >>> customer.addresses[0] = customer_address
        >>> customer.save()


Invoices
========

Example 3: Create some random invoices for the customer party. The
*invoice date* is set to sixty days before today.

.. only:: test

    Console example::

        >>> invoice_ids = []
        >>> for i in range(1,5):
        ...     day = i
        ...     invoice_date = today - datetime.timedelta(days=60)
        ...     (fiscalyear,) = Fiscalyear.find([
        ...             ('start_date', '=', \
        ...                 datetime.date(invoice_date.year,1,1))])
        ...     (revenue_account,) = Account.find([
        ...             ('code', '=', '4400'),
        ...             ('fiscalyear', '=', fiscalyear.id)])
        ...     invoice = Invoice(invoice_date=invoice_date)
        ...     invoice.party = customer
        ...     invoice_line = InvoiceLine(description = 'Testing Line 1')
        ...     invoice_line.quantity = float(random.choice(range(1,10)))
        ...     invoice_line.unit_price = \
        ...             Decimal(str(float(random.choice(range(100,1000)))))
        ...     invoice_line.account = revenue_account
        ...     invoice.lines.append(invoice_line)
        ...     invoice.save()
        ...     invoice.reload()
        ...     invoice_ids += [invoice.id]


Vouchers
========


Opening invoices with a payment term for *collective invoices*, make them
to *vouchers* of a collective invoice.

.. only:: test

    Console example::

        >>> for id in invoice_ids:
        ...     _ = Invoice.workflow_trigger_validate(id, 'open', \
        ...             config.context)
        >>> # TODO: Check for invoice type == 'voucher'

.. note:: A *voucher* is not represented by an open item in accounting.
    Vouchers are as long neutral to the balance, as they are not collected in
    a *collective invoice*.


Collective Invoice
==================

*Generate collective invoices* to combine all vouchers of a party to
collective invoices. The wizard will include all vouchers with an invoice
date before the current month. The wizard will group the vouchers by the
fields 'type', 'company', 'currency' and the months of the effective date of
the invoice. The effective date is either the value of the fields
'time_of_supply_end', 'accounting_date' or 'invoice_date'. The first value
that is not False will be taken.

.. only:: test

    Console example::

        >>> CollectiveInvoiceWizard = \
        ...         Wizard('account.invoice.create_collective_invoice')
        >>> collective_invoices = Invoice.find([
        ...         ('settlement_type', '=', 'collective'),
        ...         ('state', '=', 'draft'),
        ...         ('party', '=', customer.id)])
        >>> for invoice in collective_invoices:
        ...     _ = Invoice.workflow_trigger_validate(invoice.id, 'open', \
        ...         config.context)


